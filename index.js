
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
// let hobbies = ["Basketball","Table Tennis"," NBA2k"];
let profile1 = {

		firstName: "Rico",
		lastName: "Tasong",
		age: 28,
		
		workAddress:{
			houseNumber: "36-A",
			street: "Del Pilar",
			barangay: "Silongan",
			city: "Butuan City",
		}

	};
		

// 	console.log("First name: " + profile.firstName);
// 	console.log("Last Name: " + profile.lastName);
// 	console.log("Age: " + profile.age);
// 	console.log("Hobbies: ");
// 	console.log(hobbies);
// 	console.log("Work Address: ", profile.workAddress.houseNumber,", ", profile.workAddress.street, ", ", profile.workAddress.barangay, ", ", profile.workAddress.city);
// 	let age = 40;

	
// 	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
// 	console.log("My Friends are: ")
	

// 	let profile2 = {

// 		username: "captain_america",
// 		fullName: "Steve Rogers",
// 		bestFriend: "Bucky Barnes",
// 		age: 40,
// 		isActive: false,
// 		arrayObject: {

// 			username1: "captain_america",
// 			fullname1: "Steve Rogers",
// 			 age1: 40,
// 			 isActive1: false 
// 		}
// 	};



// 	console.log("My Full Profile: ",profile2.fullName);
// 	console.log("My current age is: " + profile2.age);
// 	console.log("My friends are: ");
// 	console.log(friends);
// 	console.log("My Full Profile: ");
// 	console.log("username: ", profile2.arrayObject.username1,"fullname: ", profile2.arrayObject.fullname1,"age: ",
// 				 profile2.arrayObject.age1,"isActive: ", profile2.arrayObject.isActive1,);
// 	console.log("My friends are: ", profile2.bestFriend);

// 	const lastLocation = "Arctic Ocean";
	
// 	console.log("I was found frozen in: " + lastLocation);


	let firstname = "Rico";
	let lastname = "Tasong";
	let hobbies = ["Basketball","Table Tennis"," NBA2k"];
	
	console.log("First Names: " + firstname);
	console.log("last Name: " + lastname);

	let myAge = 28;
	console.log("Age: " + myAge);
	console.log("Hobiies:");
	console.log(hobbies);
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("Work address:");
	console.log(profile1.workAddress);


	

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full name is: " + profile.fullName);
	console.log("My current age is: " + profile.age);

	console.log("My Friends are: ");	
	console.log(friends);
	console.log("My Full Profile:");
	console.log(profile);
	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);
